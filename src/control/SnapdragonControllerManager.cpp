/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "SnapdragonControllerManager.hpp"
#include "SnapdragonUtils.hpp"
#include "SnapdragonDebugPrint.h"

Snapdragon::ControllerManager::ControllerManager() {
	initialized_ = false;
}

Snapdragon::ControllerManager::~ControllerManager(){
	// TODO: add destructor
}

void Snapdragon::ControllerManager::Initialize(const Snapdragon::ControllerManager::InitParams& smc_params){
	smc_params_ = smc_params;
	smc_state_ = attState();
    smc_des_ = desiredAttState();
	initialized_ = true;
	rI = Integrator(); pI = Integrator(); yI = Integrator();
}

void Snapdragon::ControllerManager::updateDesiredAttState( desiredAttState &desState, desiredAttState newdesState){
	// Lock thread
  	std::lock_guard<std::mutex> lock( sync_mutex_ );
  	desState = newdesState;
}

void Snapdragon::ControllerManager::updateAttState( attState &attState, Quaternion q, Vector w){
	// Lock thread
  	std::lock_guard<std::mutex> lock( sync_mutex_ );
  	attState.q = q;
  	attState.w = w;
}

void Snapdragon::ControllerManager::updateMotorCommands ( Snapdragon::ControllerManager::motorForces &forces, desiredAttState desState, attState attState ){
	if ( desState.attStatus == 1 && initialized_ ){
		// Lock thread
	  	std::lock_guard<std::mutex> lock( sync_mutex_ );

		float f[4];
		int32_t sgn = 1;

		Quaternion qe;

		qConjProd(qe,attState.q,desState.q);
		
		if (qe.w<0)sgn=-1;

		float pE = desState.w.x-attState.w.x;
		float qE = desState.w.y-attState.w.y;
		float rE = desState.w.z-attState.w.z;

		rI.increment(sgn*qe.x,smc_params_.controlDT);
		pI.increment(sgn*qe.y,smc_params_.controlDT);
		yI.increment(sgn*qe.z,smc_params_.controlDT);

		float rollCmd  = smc_params_.Kp[0]*sgn*qe.x + smc_params_.Kd[0]*pE + smc_params_.Ki[0]*rI.value;
		float pitchCmd = smc_params_.Kp[1]*sgn*qe.y + smc_params_.Kd[1]*qE + smc_params_.Ki[1]*pI.value;
		float yawCmd   = smc_params_.Kp[2]*sgn*qe.z + smc_params_.Kd[2]*rE + smc_params_.Ki[2]*yI.value;

		// Forces calculation
		f[0] =  rollCmd - pitchCmd + yawCmd;
		f[1] =  rollCmd + pitchCmd - yawCmd;
		f[2] = -rollCmd + pitchCmd + yawCmd;
		f[3] = -rollCmd - pitchCmd - yawCmd;

		// Add throttle
		f[0] += desState.throttle;
		f[1] += desState.throttle;
		f[2] += desState.throttle;
		f[3] += desState.throttle;

		// Saturate motor commands
		f[0] = saturate(f[0],0.0,1.0);
		f[1] = saturate(f[1],0.0,1.0);
		f[2] = saturate(f[2],0.0,1.0);
		f[3] = saturate(f[3],0.0,1.0);

		// Update forces struct
		forces.f[0] = f[0];
		forces.f[1] = f[1];
		forces.f[2] = f[2];
		forces.f[3] = f[3];

		// Unused PWM pins -> set to zero
		forces.f[4] = 0.;
		forces.f[5] = 0.;
		forces.f[6] = 0.;
		forces.f[7] = 0.;

		// Update smc_data struct
		smc_data_.q_des = desState.q;
		smc_data_.q_act = attState.q;
		smc_data_.q_err = qe;

		smc_data_.w_des = desState.w;
		smc_data_.w_act = attState.w;

		smc_data_.w_err.x = pE; smc_data_.w_err.y = qE; smc_data_.w_err.z = rE;
	}
	else {
		forces.f[0] = 0.;
		forces.f[1] = 0.;
		forces.f[2] = 0.;
		forces.f[3] = 0.;
		forces.f[4] = 0.;
		forces.f[5] = 0.;
		forces.f[6] = 0.;
		forces.f[7] = 0.;
	}

}
