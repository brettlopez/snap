/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/buffer.h>

// Custom messages
#include <acl_msgs/QuadAttCmd.h>
#include <acl_msgs/IMU.h>
#include <acl_msgs/State.h>
#include <acl_msgs/SMCData.h>
#include <acl_msgs/Motors.h>

#include "SnapdragonObserverManager.hpp"


/**
 * Wrapper Ros Node to support observer from Snapdragon flight platform.
 */
namespace Snapdragon {
  namespace RosNode {
    class SND;
  }
}

/**
 * Ros SND Node that uses Snapdragon platform to get SND pose data.
 */
class Snapdragon::RosNode::SND
{
public:
  /**
   * Constructor.
   * @param nh
   *   Ros Node handle to intialize the node.
   */
  SND( ros::NodeHandle nh );

  /**
   * Start the SND node processing.
   * @return int32_t
   *  0 = success
   * otherwise = false;
   **/
  int32_t Start();

  /**
   * Stops the SND processing thread.
   * @return int32_t
   *  0 = success;
   * otherwise = false;
   **/
  int32_t Stop();


  /**
   * Destructor for the node.
   */
  ~SND();

  float lin_acc[3], ang_vel[3];
  uint32_t sequence_number;
  uint64_t current_timestamp_ns;

private: 
  // class methods
  void   PublishIMUData( Snapdragon::ObserverManager::Data ukf_data );
  void   PublishStateData( Snapdragon::ObserverManager::State ukf_state );
  void   PublishMotorCommands ( Snapdragon::ControllerManager::motorForces forces );
  void   PublishSMCData( Snapdragon::ControllerManager::controlData data );
  void   BroadcastTF ( Snapdragon::ObserverManager::State ukf_state );
  void   poseCB(const geometry_msgs::PoseStamped& msg);
  void   goalCB(const acl_msgs::QuadAttCmd& msg);
  void   pubCB (const ros::TimerEvent& e);
  void   getParams(Snapdragon::ObserverManager::InitParams& Params, Snapdragon::ControllerManager::InitParams& SmcParams);
  void   vec2ROS( geometry_msgs::Vector3& vros, Vector v);
  void   quat2ROS( geometry_msgs::Quaternion& qros, Quaternion q);
  void   ROS2vec(Vector& v, geometry_msgs::Point vros);
  void   ROS2vec(Vector& v, geometry_msgs::Vector3 vros);
  void   ROS2quat(Quaternion& q,geometry_msgs::Quaternion qros);

  // data members
  std::string quad_name_;
  Snapdragon::ObserverManager observer_man_;
  ros::NodeHandle   nh_;
  ros::Publisher    pub_imu_;
  ros::Publisher    pub_state_;
  ros::Publisher    pub_motor_;
  ros::Publisher    pub_smc_;
  ros::Subscriber   sub_pose_;
  ros::Subscriber   sub_attCmd_;
  ros::Timer        pubDataTimer_ ;
  tf2_ros::TransformBroadcaster br_;

};
