/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "SnapdragonRosNode.hpp"
#include "SnapdragonDebugPrint.h"
#include "SnapdragonUtils.hpp"

Snapdragon::RosNode::SND::SND( ros::NodeHandle nh ) : nh_(nh)
{
  // Get vehicle name
  quad_name_ = ros::this_node::getNamespace();
  // Erase slashes
  quad_name_.erase(0,2);
  pub_imu_   = nh_.advertise<acl_msgs::IMU>("imu",1);
  pub_state_ = nh_.advertise<acl_msgs::State>("state",1);
  pub_motor_ = nh_.advertise<acl_msgs::Motors>("motors",1);
  pub_smc_   = nh_.advertise<acl_msgs::SMCData>("smc",1);

  sub_pose_   = nh_.subscribe("pose", 1, &Snapdragon::RosNode::SND::poseCB, this);
  sub_attCmd_ = nh_.subscribe("attCmd", 1, &Snapdragon::RosNode::SND::goalCB, this);

  pubDataTimer_ = nh_.createTimer(ros::Duration(0.005), &Snapdragon::RosNode::SND::pubCB, this);

  // sleep here so tf buffer can get populated
  ros::Duration(1).sleep(); // sleep for 1 second
}

Snapdragon::RosNode::SND::~SND()
{
  Stop();
}

int32_t Snapdragon::RosNode::SND::Start() {
  Snapdragon::ObserverManager::InitParams Params;
  Snapdragon::ControllerManager::InitParams SmcParams;

  getParams(Params, SmcParams);

  if( observer_man_.Initialize(Params, SmcParams) != 0  ) {
    ROS_WARN_STREAM( "Snapdragon::RosNode: Error initializing the SND Manager" );
    return -1;
  }

// start the snap processing.
  if( observer_man_.Start() != 0 ) {
    ROS_WARN_STREAM( "Snapdragon::RosNode: Error initializing the SND Manager"  );
    return -1;
  }

  return 0;
}

int32_t Snapdragon::RosNode::SND::Stop(){
  // TODO: make sure other process are shutdown properly
}

void Snapdragon::RosNode::SND::pubCB (const ros::TimerEvent& e){
  // pub stuff
  if (observer_man_.calibrated_){
    PublishIMUData(observer_man_.imu_data_); 
    PublishStateData(observer_man_.state_);
    PublishMotorCommands(observer_man_.smc_motors_);
    PublishSMCData(observer_man_.smc_data_);
    BroadcastTF(observer_man_.state_);
  }
}

void Snapdragon::RosNode::SND::PublishIMUData( Snapdragon::ObserverManager::Data imu_data  ) {
  acl_msgs::IMU imu_msg;
  ros::Time frame_time;
  frame_time.sec = (int32_t)(imu_data.current_timestamp_ns/1000000000UL);
  frame_time.nsec = (int32_t)(imu_data.current_timestamp_ns % 1000000000UL);
  imu_msg.header.frame_id = "body";
  imu_msg.header.stamp = ros::Time::now();
  imu_msg.imu_stamp = frame_time;
  imu_msg.header.seq = imu_data.sequence_number;
  imu_msg.accel.x = imu_data.lin_accel[0];
  imu_msg.accel.y = imu_data.lin_accel[1];
  imu_msg.accel.z = imu_data.lin_accel[2];
  imu_msg.gyro.x = imu_data.ang_vel[0];
  imu_msg.gyro.y = imu_data.ang_vel[1];
  imu_msg.gyro.z = imu_data.ang_vel[2];

  pub_imu_.publish(imu_msg);
}

void Snapdragon::RosNode::SND::PublishStateData( Snapdragon::ObserverManager::State state ){
  acl_msgs::State state_msg;
  ros::Time frame_time;
  frame_time.sec = (int32_t)(state.current_timestamp_ns/1000000000UL);
  frame_time.nsec = (int32_t)(state.current_timestamp_ns % 1000000000UL);
  state_msg.header.frame_id = "world";
  state_msg.header.stamp = ros::Time::now();
  state_msg.header.seq = state.sequence_number;
  state_msg.state_stamp = frame_time;
  vec2ROS(state_msg.pos,state.pos);
  vec2ROS(state_msg.vel,state.vel);
  quat2ROS(state_msg.quat,state.q);
  vec2ROS(state_msg.w,state.w);
  vec2ROS(state_msg.abias,state.accel_bias);
  vec2ROS(state_msg.gbias,state.gyro_bias);
  pub_state_.publish(state_msg);
}

void Snapdragon::RosNode::SND::PublishMotorCommands( Snapdragon::ControllerManager::motorForces forces ){
  static int count = 0;
  acl_msgs::Motors motor_msg;
  motor_msg.header.stamp = ros::Time::now();
  motor_msg.header.seq = count;
  motor_msg.m1 = forces.f[0];
  motor_msg.m2 = forces.f[1];
  motor_msg.m3 = forces.f[2];
  motor_msg.m4 = forces.f[3];
  motor_msg.m5 = forces.f[4];
  motor_msg.m6 = forces.f[5];
  motor_msg.m7 = forces.f[6];
  motor_msg.m8 = forces.f[7];
  
  pub_motor_.publish(motor_msg);
  count++;
}

void Snapdragon::RosNode::SND::PublishSMCData( Snapdragon::ControllerManager::controlData data ){
  acl_msgs::SMCData msg;
  msg.header.stamp = ros::Time::now();
  quat2ROS(msg.q_des,data.q_des);
  quat2ROS(msg.q_act,data.q_act);
  quat2ROS(msg.q_err,data.q_err);
  vec2ROS(msg.w_des,data.w_des);
  vec2ROS(msg.w_act,data.w_act);
  vec2ROS(msg.w_err,data.w_err);
  vec2ROS(msg.s,data.s);

  pub_smc_.publish(msg);
}

void Snapdragon::RosNode::SND::BroadcastTF ( Snapdragon::ObserverManager::State state ){
  geometry_msgs::TransformStamped transformStamped;
 
  float norm = sqrt(pow(state.q.w,2) + pow(state.q.x,2) + pow(state.q.y,2) + pow(state.q.z,2)); 
 
  transformStamped.header.stamp = ros::Time::now();
  transformStamped.header.frame_id = "world";
  transformStamped.child_frame_id = quad_name_;
  transformStamped.transform.translation.x = state.pos.x;
  transformStamped.transform.translation.y = state.pos.y;
  transformStamped.transform.translation.z = state.pos.z;
  transformStamped.transform.rotation.x = state.q.x/norm;
  transformStamped.transform.rotation.y = state.q.y/norm;
  transformStamped.transform.rotation.z = state.q.z/norm;
  transformStamped.transform.rotation.w = state.q.w/norm;

  br_.sendTransform(transformStamped);
}

void Snapdragon::RosNode::SND::poseCB(const geometry_msgs::PoseStamped& msg){
  if (observer_man_.calibrated_){
    Vector pos; Quaternion q;
    ROS2vec(pos,msg.pose.position);
    ROS2quat(q,msg.pose.orientation);
    observer_man_.updateState(observer_man_.state_,pos,q);
  }
}

void Snapdragon::RosNode::SND::goalCB(const acl_msgs::QuadAttCmd& msg){
  if (observer_man_.calibrated_){
    // Update smc command
    desiredAttState newdesState;
    newdesState.attStatus = msg.att_status;
    newdesState.throttle  = msg.throttle;
    ROS2quat(newdesState.q,msg.att);
    ROS2vec(newdesState.w, msg.rate);
    observer_man_.updateSMCState(newdesState);
  }
}

void Snapdragon::RosNode::SND::getParams(Snapdragon::ObserverManager::InitParams& Params, Snapdragon::ControllerManager::InitParams& SmcParams){
  // Filter params
  double theta;
  double kAtt;
  double kGyroBias;
  double kAccelBias;
  double DT;
  double controlDT;
  
  ros::param::param<double>("~theta",theta,0.8);
  ros::param::param<double>("~kAtt",kAtt,0.0018);
  ros::param::param<double>("~kGyroBias",kGyroBias,0.0002);
  ros::param::param<double>("~kAccelBias",kAccelBias,0.01);
  ros::param::param<double>("~DT",DT,0.01);
  ros::param::param<double>("~controlDT",controlDT,0.002);

  SmcParams.controlDT = controlDT;

  Params.Kp = 1-std::pow(theta,3);
  Params.Kv = 1.5*(1-std::pow(theta,2))*(1-theta)/DT;
  Params.Kq = kAtt;
  Params.Kab = kAccelBias;
  Params.Kgb = kGyroBias;

  double b; ros::param::param<double>("~b",b,0.3);
  double l; ros::param::param<double>("~l",l,0.15);
  double c; ros::param::param<double>("~c",c,0.02);
  double Jx; ros::param::param<double>("~Jx",Jx,0.1);
  double Jy; ros::param::param<double>("~Jy",Jy,0.1);
  double Jz; ros::param::param<double>("~Jz",Jz,0.1);

  SmcParams.b=b;
  SmcParams.l = 1/(4*l);
  SmcParams.c = 1/(4*c);

  // PD params
  double kpr; ros::param::param<double>("~Kp/roll",kpr,400.0);
  double kdr; ros::param::param<double>("~Kd/roll",kdr,150.0);
  double kir; ros::param::param<double>("~Ki/roll",kir,150.0);

  double kpp; ros::param::param<double>("~Kp/pitch",kpp,400.0);
  double kdp; ros::param::param<double>("~Kd/pitch",kdp,150.0);
  double kip; ros::param::param<double>("~Ki/pitch",kip,150.0);

  double kpy; ros::param::param<double>("~Kp/yaw",kpy,600.0);
  double kdy; ros::param::param<double>("~Kd/yaw",kdy,200.0);
  double kiy; ros::param::param<double>("~Ki/yaw",kiy,200.0);

  SmcParams.Kp[0]=kpr/1000.0; SmcParams.Kp[1]=kpp/1000.0; SmcParams.Kp[2]=kpy/1000.0;
  SmcParams.Kd[0]=kdr/1000.0; SmcParams.Kd[1]=kdp/1000.0; SmcParams.Kd[2]=kdy/1000.0;  
  SmcParams.Ki[0]=kir/1000.0; SmcParams.Ki[1]=kip/1000.0; SmcParams.Ki[2]=kiy/1000.0;  

  double a_xy, a_z, g;
  ros::param::param<double>("~alpha/a_xy",a_xy,0.5);
  ros::param::param<double>("~alpha/a_z",a_z,0.5);
  ros::param::param<double>("~alpha/g",g,0.5);

  observer_man_.acc_xy_lpf = 1-a_xy;
  observer_man_.acc_z_lpf  = 1-a_z;
  observer_man_.gyro_lpf   = 1-g;

  saturate(observer_man_.acc_xy_lpf,0.0,1.0);
  saturate(observer_man_.acc_z_lpf,0.0,1.0);
  saturate(observer_man_.gyro_lpf,0.0,1.0);

}

void Snapdragon::RosNode::SND::vec2ROS(geometry_msgs::Vector3& vros, Vector v){
  vros.x = v.x;
  vros.y = v.y;
  vros.z = v.z;
}

void Snapdragon::RosNode::SND::ROS2vec(Vector& v, geometry_msgs::Point vros){
  v.x = vros.x;
  v.y = vros.y;
  v.z = vros.z;
}

void Snapdragon::RosNode::SND::ROS2vec(Vector& v, geometry_msgs::Vector3 vros){
  v.x = vros.x;
  v.y = vros.y;
  v.z = vros.z;
}

void Snapdragon::RosNode::SND::quat2ROS(geometry_msgs::Quaternion& qros, Quaternion q){
  qros.w = q.w;
  qros.x = q.x;
  qros.y = q.y;
  qros.z = q.z;
}

void Snapdragon::RosNode::SND::ROS2quat(Quaternion& q,geometry_msgs::Quaternion qros){
  q.w = qros.w;
  q.x = qros.x;
  q.y = qros.y;
  q.z = qros.z;
}



