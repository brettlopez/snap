/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "SnapdragonCommManager.hpp"
#include "SnapdragonDebugPrint.h"

Snapdragon::CommManager::CommManager() {
	initialized_ = false;
}

Snapdragon::CommManager::~CommManager(){
	// TODO: add destructor
}

int32_t Snapdragon::CommManager::Initialize(){
	const int shared_segment_size = sizeof(tMotorpacket);
	/* Allocate a shared memory segment. */
	segment_id_pkt_ = shmget (shm_key_pkt_, shared_segment_size, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
	pkt_ = (tMotorpacket*) shmat (segment_id_pkt_, 0, 0);

	// Shared memory mutex
	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
	pthread_mutex_init(&pkt_->ipc_mutex, &mutex_attr);

	pthread_condattr_t cond_attr;
	pthread_condattr_init(&cond_attr);
	pthread_condattr_setpshared(&cond_attr, PTHREAD_PROCESS_SHARED);
	pthread_cond_init(&pkt_->ipc_condvar, &cond_attr);

	initialized_ = true;

	// TODO: add check that shared memeory was created correctly

}

void Snapdragon::CommManager::updateMemory(float f[8]){
	static uint32_t seq = 0;
	if (initialized_){
		int f_pwm[8];
		// Convert motor force to PWM
		f_pwm[0] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[0] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[1] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[1] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[2] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[2] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[3] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[3] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[4] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[4] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[5] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[5] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[6] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[6] + PWM_MINIMUM_PULSE_WIDTH;
		f_pwm[7] = (int) (PWM_MAXIMUM_PULSE_WIDTH - PWM_MINIMUM_PULSE_WIDTH)*f[7] + PWM_MINIMUM_PULSE_WIDTH;

		// Lock thread
		pthread_mutex_lock(&pkt_->ipc_mutex);
	   	pkt_->seq = seq;
	   	pkt_->f[0]=f_pwm[0];
	   	pkt_->f[1]=f_pwm[1];
	   	pkt_->f[2]=f_pwm[2];
	   	pkt_->f[3]=f_pwm[3];
	   	pkt_->f[4]=f_pwm[4];
	   	pkt_->f[5]=f_pwm[5];
	   	pkt_->f[6]=f_pwm[6];
	   	pkt_->f[7]=f_pwm[7];
	   	
		// Trigger thread conditional 
        pthread_cond_signal(&pkt_->ipc_condvar);
        // Unlock thread
        pthread_mutex_unlock(&pkt_->ipc_mutex);
        seq ++; 
	}
}
	

int32_t Snapdragon::CommManager::Terminate(){
	if (initialized_){
		pthread_mutex_destroy(&pkt_->ipc_mutex);
		pthread_cond_destroy(&pkt_->ipc_condvar);

		/* Detach the shared memory segment. */
		shmdt (pkt_);
		/* Deallocate the shared memory segment.*/
		shmctl (segment_id_pkt_, IPC_RMID, 0);
	}
}
